# HEU选课（补选阶段测试，正选也可用）

## 免责声明

运行脚本产生的一切后果由运行者承担，本人概不负责。

推荐IDE：pycharm


**使用指南：**

## 1.进入选课官网，输入账号密码等，点击登录

## 2.打开浏览器开发者工具（F12）

## 3.刷新页面，找到list数据包请求标头的Authorization和Batchid

![](https://gitee.com/myclms/pictures/raw/master/20240227141455.png)

![Authorization部分截图](https://gitee.com/myclms/pictures/raw/master/20240723112948.png)



## 4.根据代码头部注释修改相关参数

![](https://gitee.com/myclms/pictures/raw/master/20240227134906.png)

![](https://gitee.com/myclms/pictures/raw/master/20240227134932.png)

## 5.运行代码

控制台输出的数字代表正在遍历的页面号(变量名：page, line 150)；

抢成功会输出抢到的课程名字(line 141);

抢到设置数量的课程后程序才会停止，正常停止前会输出这次程序运行所有抢到的课。(line 156)



**一些出现过的问题：**

1. 控制台输出‘请求过快’ -- 可能是max_page与网站最新数据不符；或者cookie过期，重新复制Authorization再试试。
2. 控制台输出'管理员变更数据或账号在其他地方登录，请重新登录' -- 可能是cookie需要更新。
3. 一般问题重新运行就能解决。
4. 环境（第三方库）问题。
5. ...



## 后言

代码只用到了基础的python爬虫知识。欢迎修改代码，一些性能等方面能够优化或者有问题的地方请提出issues。

写代码的初衷就是因为选课经常不中，翻看未满课程太费事。

希望学校选课网站制作者能增加查看未满课程的筛序项；增强网站加密程度，防止恶意抢课发生。






# 源码

```python
# 补选抢课 6：00 ~ 22：00
# 需要定制的：Authorization max_page(公选课对应类型最大页面数) num(想抢的课程数) want_type(公选课类型) key(课程名中的关键字（记得改对应的最大页数）)
# !!!Authorization，batchid,max_page!!!
import requests
import json
import jsonpath
import time

# 加上这行代码即可，关闭安全请求警告
requests.urllib3.disable_warnings()

# # #
num = 1
max_page = 2
pageSize = 2  # 一页设置成了2个课程记录 -- 1.优点：实时性更强，防止终端输出抢课成功，网页上抢不到课；2.缺点：real_num_of_pages
# 可能不准，可以自己算一下，然后修改real_num_of_pages；遍历所有课程耗时、次数也更多;请求次数变多也可能封IP。
batchid = '5457852c392e4fb0bc162402c1047701'
Authorization = ''
want_type = ''  # A,B,C,~,A0分别对应12,13,14,~,18;空字符串代表全部类型（不是空格）。
key = "网络"  # 课程名中的关键字（记得改对应的最大页数）选网课，就可以写 -- 网络
time_sleep = 0.5  # 请求不同页之间的休眠时间，防止请求过快
# # #

assert (10 >= pageSize > 0)
assert (5 >= num > 0)
assert (want_type != ' ')
assert (time_sleep >= 0.5)
real_num_of_pages = max_page * (10 // pageSize)  # 10是原来一页的课程数量
print('real_num_of_pages:', real_num_of_pages)
XK_api = 'https://jwxk.hrbeu.edu.cn/xsxk/elective/clazz/list'
grab_api = 'https://jwxk.hrbeu.edu.cn/xsxk/elective/hrbeu/add'
cookies = {
    'Authorization': Authorization
}
headers = {
    'Accept': 'application/json, text/plain, */*',
    'Accept-Language': 'zh-CN,zh;q=0.9,en;q=0.8,en-GB;q=0.7,en-US;q=0.6',
    'Authorization': Authorization,
    'Connection': 'keep-alive',
    'Content-Type': 'application/json;charset=UTF-8',
    'Origin': 'https://jwxk.hrbeu.edu.cn',
    'Referer': 'https://jwxk.hrbeu.edu.cn/xsxk/elective/grablessons?batchId=' + batchid,
    'Sec-Fetch-Dest': 'empty',
    'Sec-Fetch-Mode': 'cors',
    'Sec-Fetch-Site': 'same-origin',
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36 Edg/121.0.0.0',
    'batchId': batchid,
    'sec-ch-ua': '"Not A(Brand";v="99", "Microsoft Edge";v="121", "Chromium";v="121"',
    'sec-ch-ua-mobile': '?0',
    'sec-ch-ua-platform': '"Windows"',
}
headers1 = {
    'Accept': 'application/json, text/plain, */*',
    'Accept-Language': 'zh-CN,zh;q=0.9,en;q=0.8,en-GB;q=0.7,en-US;q=0.6',
    'Authorization': Authorization,
    'Connection': 'keep-alive',
    'Content-Type': 'application/x-www-form-urlencoded',
    'Origin': 'https://jwxk.hrbeu.edu.cn',
    'Referer': 'https://jwxk.hrbeu.edu.cn/xsxk/elective/grablessons?batchId=' + batchid,
    'Sec-Fetch-Dest': 'empty',
    'Sec-Fetch-Mode': 'cors',
    'Sec-Fetch-Site': 'same-origin',
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/121.0.0.0 Safari/537.36 Edg/121.0.0.0',
    'batchId': batchid,
    'sec-ch-ua': '"Not A(Brand";v="99", "Microsoft Edge";v="121", "Chromium";v="121"',
    'sec-ch-ua-mobile': '?0',
    'sec-ch-ua-platform': '"Windows"',
}

finished = False
goods = []
lesson = {}
cnt = 0

while not finished:
    cnt += 1
    print('\n', '第', cnt, '次循环')
    for page in range(1, int(real_num_of_pages + 1)):
        # print('page')
        # page = 4
        json_data = {
            'teachingClassType': 'XGKC',
            'pageNumber': page,
            'pageSize': pageSize,
            'orderBy': '',
            'campus': '01',
            'SFCT': '0',
            'XGXKLB': want_type,  # A~a0 12~18,
            'KEY': key,
        }
        # json_data = {
        #     'XGXKLB': '18',#A~a0 12~18,
        #
        # }
        # 请求课程列表（一页）
        response = requests.post(XK_api, cookies=cookies, headers=headers,
                                 json=json_data, verify=False)

        # print(response.text)
        # 分析数据
        res_dict = json.loads(response.text)
        while res_dict['code'] != 200:
            print(res_dict['code'], res_dict['msg'])
            time.sleep(10 * time_sleep)
            response = requests.post(XK_api, cookies=cookies,
                                     headers=headers, json=json_data, verify=False)

        names = jsonpath.jsonpath(res_dict, '$.data.rows.*.KCM')
        scores = jsonpath.jsonpath(res_dict, '$..XF')
        maxs = jsonpath.jsonpath(res_dict, '$..classCapacity')
        nows = jsonpath.jsonpath(res_dict, '$..numberOfSelected')
        classids = jsonpath.jsonpath(res_dict, '$..JXBID')
        vals = jsonpath.jsonpath(res_dict, '$..secretVal')
        ss = jsonpath.jsonpath(res_dict, '$..SFYX')
        # print(ss)
        # print(vals,len(vals))
        # print(classids,len(classids))
        # print(len(names))
        # print(names,len(names))
        # print(scores)
        # print(maxs)
        # print(nows)
        if type(ss) != type([]):
            continue
        for i in range(0, len(scores)):

            # 如果课程人数未满
            if nows[i] < maxs[i]:
                if ss[i] == '1':
                    # 选过了
                    continue
                else:
                    # 选它！！！
                    data = {
                        'clazzType': 'XGKC',
                        'clazzId': classids[i],
                        'secretVal': vals[i],
                    }
                    # 发送选课请求
                    res = requests.post(grab_api, cookies=cookies,
                                        headers=headers1, data=data, verify=False)

                    res_dict1 = json.loads(res.text)
                    print('\n', names[i], ':', res_dict1['code'], res_dict1['msg'])
                    if res_dict1['code'] == 200:
                        # 选成功的记录一下
                        lesson['name'] = names[i]
                        lesson['score'] = scores[i]
                        goods.append(lesson)
                        if len(goods) >= num:
                            finished = True
                            break

        print(page, end=' ')
        if finished:
            break
        # 防止请求过快
        time.sleep(time_sleep)

# 总结
print('')
if len(goods):
    print('已选到课程：')
    for good in goods:
        print(good)
else:
    print('发！发！发！')

```

