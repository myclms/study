### 1. 链接

1. 学习

[Unity入门使用说明 - Feishu Docs](https://datawhaler.feishu.cn/wiki/WwPuw58EPiWb93k5yvRccHgvnXc)

[][Unity教程][使用Unity制作日式RPG游戏](https://www.bilibili.com/video/BV1hz4y1376C?p=4&vd_source=858893c79cd58f7ceaa6831fb2deaa57)

coroutine 协程

2. 资源

[46+ 个免费游戏素材网站整理：2D、3D、人物、模型等免费下载 | Eagle 博客](https://cn.eagle.cool/blog/post/free-game-assets)

[Top free game assets - itch.io](https://itch.io/game-assets/free)

### 2. 内容总结

1. **Background**    png -> **tile pallete** 

2. **Player**    

   1. idle and walking **animation** made using unity editor specific tab.
   2. **sate tree** to order animations.
   3. C# **scripts** to help Player move with playing animation prepared (many apis of unity, grammar is similar to C). The public parameters will be set in unity editor. ' [SerializeField] ClassName InstanceName ' can make serialized parameters.

   PlayerController

3. **Collision**

   **collider2D**

   **rigid body 2D**

   ```c#
   var collider = Physics2D.OverlapCircle(interactPos, 0.2f, InteractablesLayer);
   if (collider != null)
   {
       collider.GetComponent<Interactable>()?.Interact();
   }
   ```

   Interactable : class -> **Interface** 

4. **Dialog**

  UI

  GameController

  Dialog Manager

  自定义类 Dialog : 类前面加上' [System.Serializable] ',是自定义类可以在editor 上赋值。

5. **Battle System**

  blue print -> code

6. 

     

  ​    

  ​    

