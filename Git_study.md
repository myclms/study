# [Git准备工作](https://gitee.com/jianguo888/heu/blob/master/02Git使用.md)

# Git基本使用

## 1.基本流程

1. 初始化文件夹为git可管理的仓库

   `git init`

2. 和远程库建立连接

   `git remote add origin` <u>git@gitee.com:jianguo888/heu.git</u>(origin 为名字;默认创立master分支;ssh协议，也可为http协议)

   *克隆或拉取可省略1、2*：`git clone`<u>git@gitee.com:jianguo888/heu.git</u>

   `git pull`

3. 本地修改文件

4. 修改添加到暂存区

   `git add .`(所有修改)

   `git add 01.md`(只有01.md提交)

5. 暂存区内容提交到工作栈

   `git commit -m`"修改描述信息"

6. commit内容上传到远程库

   `git push -u origin master `(第一次push用-u,后续省略)

7. 过程中可以查看修改日志、暂存区等状态

   `git status`

   `git remote -v`

   `git log`

   


## [2.时光穿梭机](https://www.liaoxuefeng.com/wiki/896043488029600/896954074659008)



