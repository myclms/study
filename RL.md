

# RL

classic,basic->practical,effective.

types:

* needed: **data** or **model**;

* model-based(1~4) or **model-free**(5~10).

target: look for optimal policy!.

path when being model-free:

a stochastic policy -> considering data,iterations are done for reduce the error and optimize the policy -> optimal policy

map:

![](https://gitee.com/myclms/pictures/raw/master/20240410195800.png)

## 1.Basic Concepts

### 1)grid-world example

#### **State**:

The status of agent with respect to the environment.

* For the grid-world example,<u>the location</u> of the agent is the state.
* **State space**:the set of all states.  $$S = {\{s_i\}}^9_{i=1}$$

#### **Action**:

<u>For every state</u>,there are some actions that agent can do.

* For the grid-world example,in one state,the agent can(namely its actions are) move upwards,move rightwards,stay unchanged,...

* **Action space of a state**

   $A(s_i)={\{a_i\}}^5_{i=1}$

* Different states have differnt actions.$A$ is a function about $s_i$

#### **State transition**:

When taking an action,the agent may <u>move from one state to another</u>.This process is called transition.

#### **Forbidden area**:

* accessible,but with penalty.

* inaccessible.

* presentation:

  * (**Tabular represention** - can only represent deterministic cases.)

  * **State transition probability**:use probability to describe state transition.(conditional probability)

* a <u>deterministic</u>(oppsition to <u>stochastic</u>) case:

  $$p(s_2|s_1,a_2)=1\\p(s_i|s_1,a_2)=0\ \ \ \forall i \not=2$$

  

#### **Policy**:

tell the agent what actions to take at a state.(conditional probability)

* a deterministic case:

  $$\pi(a_1|s_1)=0\\\pi(a_2|s_1)=1\\\pi(a_3|s_1)=0\\\pi(a_4|s_1)=0\\\pi(a_5|s_1)=0$$

* presentation:

  ![](https://gitee.com/myclms/pictures/raw/master/20240321191944.png)

#### **Reward**:

a real number we get after taking an action.can be interpreted as **a human-machine interface**.

* A positive reward -- encouragement

* A negative reward -- punishment

* a example:

  $$r_{bound}=-1\\r_{forbbiden}=-1\\r_{target}=1\\r_{others}=0$$

* presentation:

  $$p(r=-1|s_1,a_1)=1\ and\ p(r\not=-1|s_1,a_1)=0$$

* depends on the current state and the selected action.

#### Trajectory and return:

![](https://gitee.com/myclms/pictures/raw/master/20240321193605.png)

* return decides which trajectory is better.

* **Discounted return**

  ![](https://gitee.com/myclms/pictures/raw/master/20240321194204.png)

#### Episode/Trial:

a finite trajectory.

* a task with episode is called <u>episodic task</u>.otherwise,the task is called <u>continuing task</u>.

* episodic task -> continuing task

  ![](https://gitee.com/myclms/pictures/raw/master/20240321194906.png)

### 2)Summary in Markov decision process

![](https://gitee.com/myclms/pictures/raw/master/20240321195350.png)

## 2.Bellman Equation

### 1)the core idea

the value of one state relies on the values of other states.

### 2)State value

The <u>expectation of returns</u> <u>starting from one state</u> and taking <u>different policy space</u>.

![](https://gitee.com/myclms/pictures/raw/master/20240323140945.png)

When the trajectory is determistic,state value is equal to return in math.

example:

![](https://gitee.com/myclms/pictures/raw/master/20240323141424.png)

from intuition:

$$v_{\pi_1}(s_1)=0+\gamma v_{\pi_1}(s_3),\cdots\\which\ can\ be\ seen\ as\ one\ equation\ of\ Bellman\ Equation$$

### 3)Bellman Equation:get state value

#### why?

Given a policy,we can calculate the state value of every state according to BE.Then,we can evaluate this policy which is called **policy-evaluation** and fundmental in RL.

#### derivation:

calculate E through sum of  some conditional propability.

![](https://gitee.com/myclms/pictures/raw/master/image-20240323142328888.png)

#### solution:

**matrix -vector form**

$r_\pi$ is E of immediate rewards satrting from one state;$P_\pi$ is the state transition matrix.

![](https://gitee.com/myclms/pictures/raw/master/20240323153825.png)

![](https://gitee.com/myclms/pictures/raw/master/20240323153905.png)

Then,we can transite the formula and get the answer by caculate the <u>inverse matrix</u>.But,it's not practical when the matrix's dimmension is large.So,we use an **iterative solutiion**:

![](https://gitee.com/myclms/pictures/raw/master/20240323155107.png)

(proof:define the error,we only need to show the error -> 0)

### 4)Action value

The <u>expectation of returns</u> <u>starting from one state</u> and taking <u>different policy space</u> and <u>taking an action</u>.

#### why?

We want to know which action is better.

#### definition and relationship with SV

![](https://gitee.com/myclms/pictures/raw/master/20240323160823.png)

![](https://gitee.com/myclms/pictures/raw/master/20240323160953.png)

### 5)Summary

![](https://gitee.com/myclms/pictures/raw/master/20240323161558.png)

## 3.Bellman Optimality Equation

### 1)optimal policy

$v_{\pi^*}(s) \geq v_{\pi}(s)$,for every state and for every other policy.$\pi^*$ is caled the optimal policy.

### 2)Bellman Optimality Equation

![](https://gitee.com/myclms/pictures/raw/master/20240325201153.png)

where $\pi$ is not given.

### 3)How to solve it?

**two examples suggesting how to solve the tricky equation:**

![](https://gitee.com/myclms/pictures/raw/master/20240325202433.png)

![](https://gitee.com/myclms/pictures/raw/master/20240325202612.png)

​	**how to find the $\pi$ which make the right formula maximized?** In the policy,the $\pi(a^*|s_i)=1$ ,$a^*$ makes $q(s_i,a^*)$ is biggest(given determined $v(s^,)$ ). **Greedy algorithm!** 

We can write BOE as $v=f(v)$ .

**Supplement:**

two concepts:fixed point and contraction function(mapping).

a theorem:Contraction Mapping Theorem(used to solve the equation like $x = f(x)$ ) 

![](https://gitee.com/myclms/pictures/raw/master/20240325204211.png)

**Summary:**

![](https://gitee.com/myclms/pictures/raw/master/20240325204722.png)

Finally, BOE can be written as: $v^*=r_{\pi^*}+\gamma P_{\pi^*}v_{\pi^*}$ .So,BOE is one special case of BE.

### 4)Analyzing optimal policy

factors:$r,\gamma,p(\cdots)$ 

$\gamma$ :little-shorted-sighted;big-long-sighted.

$r$ :linear change for $r$ will not change the result.

## 4.Value Iteration and Policy Iteration

### 1)Value iteration algorithm

Policy update(PU) -> Value update(VU)

The algorithm is just that introduced above in BOE.

![](https://gitee.com/myclms/pictures/raw/master/image-20240326150004860.png)

###2)Policy iteration algorithm 

![](https://gitee.com/myclms/pictures/raw/master/image-20240326145915982.png)

![](https://gitee.com/myclms/pictures/raw/master/image-20240326145936801.png)

### 3)Truncated iteration algorithm

**practical policy iteration** 

#### Compare PI and VI algorithm

![](https://gitee.com/myclms/pictures/raw/master/%E5%B1%8F%E5%B9%95%E6%88%AA%E5%9B%BE%202024-03-26%20181350.png)

#### Compromise:TI

algorithm:

![](https://gitee.com/myclms/pictures/raw/master/%E5%B1%8F%E5%B9%95%E6%88%AA%E5%9B%BE%202024-03-26%20181113.png)

VI and PI can be seen as two special cases of TI:

![](https://gitee.com/myclms/pictures/raw/master/%E5%B1%8F%E5%B9%95%E6%88%AA%E5%9B%BE%202024-03-26%20181005.png)

composition:the times ( $k$ ) needed for making $|v_{k}-v^*|$ (the error) down to an acceptable  range.

![](https://gitee.com/myclms/pictures/raw/master/%E5%B1%8F%E5%B9%95%E6%88%AA%E5%9B%BE%202024-03-26%20181321.png)

![](https://gitee.com/myclms/pictures/raw/master/%E5%B1%8F%E5%B9%95%E6%88%AA%E5%9B%BE%202024-03-26%20181953.png)

## 5.Monte Carlo Learning

<u>policy iteration</u> is converted to <u>be model-free</u>

### 1)MC Estimation

$E(x) \approx \bar x $  

Based on the **Law of Large Number** that requires the samples must be **iid** (independent and identically distributed).

### 2)MC Basic(not efficient,theoretical)

core idea:

![](https://gitee.com/myclms/pictures/raw/master/20240327154107.png)

algorithm:

![](https://gitee.com/myclms/pictures/raw/master/20240327154653.png)

Since mean,how to set **episode length** ? 

It's similar to exploring radius.Evidently,it cannot be infinitely long and too short.

### 3)MC Exploring Starts(more efficient)

Two aspects can be improved:<u>use data</u> and <u>update estimate</u> more efficiently.

**Generalized policy iteration**(GPI):an idea

![](https://gitee.com/myclms/pictures/raw/master/20240327162405.png)

algorithm:

![](https://gitee.com/myclms/pictures/raw/master/20240327162818.png)

$t$ is from T-1 to 0 for higher efficiency.

* exploring:<u>get every E(q(s,a))</u> to make sure the optimal policy is not skipped.
* start:start from one (s,a) and give an episode.

![](https://gitee.com/myclms/pictures/raw/master/20240327163544.png)

'exploring' and 'start' is not so practical!

### 4)MC $\epsilon$-Greedy(without exploring starts)

We want to use <u>a few episodes</u> even one episode to visit every (s,a).In this condition,we don't need to exploring starts ,meaning that the algorithm is more practical. 

policy:

* determistic:greedy policy
* stochastic:soft policy   $\epsilon$-greedy policy( By setting soft policy,the episode can cover every (s,a) with random sampling.Then,use every-visit way to get E(q(s,a)) . )

Why?

![](https://gitee.com/myclms/pictures/raw/master/20240327171323.png)

algorithm:

![](https://gitee.com/myclms/pictures/raw/master/20240327171720.png)

advice: $\epsilon$  can be 1 at first(explore more),then become 0 (more closed to the optimal policy) increasingly.

## 6.Stochastic Approximation

It **doesn't require to know the expression of the function** nor its derivative when finding root.

### 1)mean estimation

a new **iterated** way to calculate estimation:

![](https://gitee.com/myclms/pictures/raw/master/20240328190146.png)

it can save time.

the E is not accurate until the end , but it's better than nothing.

$\frac{1}{k}$ can be replaced into $a_k$ according to RM mentioned below.

### 2)Robbins-Monro algorithm

**target**:

get $w^*$ satisfying $g(w^*)=0,where \ g=g(w)$  

**algorithm**:

$w_{k+1} = w_k - a_k \tilde g(w_k,\eta _k), \\ where\ w_k\ is\ the\ kth\ estimation\ for\ w^*$ 



$w_k$ ( input ) -> $g(w)$  ( a black box ) -> $\tilde g(w_k,\eta _k)$ ( an output with error )

When $w_k$ will converge to $w^*$ ? 

![](https://gitee.com/myclms/pictures/raw/master/20240328193925.png)

* condition 1:g(w) is increasing and is limited

* condition 2:When $w_1$ is far away from $w^*$ ,this algorithm is also right. $w_k$ will converge to a constant.$a_k = \frac{1}{k}$ .
* condition 3:$\eta_k$ can be iid and not required to be Gaussian.

note:

![](https://gitee.com/myclms/pictures/raw/master/20240328200624.png)

### 3)Stocahstic gradient descent(SGD)

**target**:

![](https://gitee.com/myclms/pictures/raw/master/20240328202929.png)

**description**:

GD->BGD->SGD

![](https://gitee.com/myclms/pictures/raw/master/image-20240328202751787.png)

![](https://gitee.com/myclms/pictures/raw/master/20240328202718.png)

an example:

![](https://gitee.com/myclms/pictures/raw/master/image-20240328204045752.png)

where SGD formula is same as mean-expectation.

**transform it into RM**:

![](https://gitee.com/myclms/pictures/raw/master/20240328204836.png)

=g(w)

convergence patten:

![](https://gitee.com/myclms/pictures/raw/master/20240328210243.png)

$\delta_k$ is relative error.

### 4)BGD,MBGD and SGD

introduction:

![](https://gitee.com/myclms/pictures/raw/master/20240328211659.png)

relationships:

![](https://gitee.com/myclms/pictures/raw/master/20240328211628.png)

BGD isn't practical,SGD need more times to converge.MBGD is a compromise.

### 5)Summary

![](https://gitee.com/myclms/pictures/raw/master/20240328212718.png)

## 7.Temprol-Difference learning

### 1)TD learning of state value(given one policy)

estimate state value --> <u>policy evaluation</u>

<u>solve BE without model</u>( with data )

**algorithm:**

![](https://gitee.com/myclms/pictures/raw/master/20240403154004.png)

![](https://gitee.com/myclms/pictures/raw/master/20240403154052.png)

**TD target:**

![](https://gitee.com/myclms/pictures/raw/master/20240403154157.png)

**TD error:**

![](https://gitee.com/myclms/pictures/raw/master/20240403154259.png)



### 2) Comparison between TD and MC

![](https://gitee.com/myclms/pictures/raw/master/20240403160155.png)

![](https://gitee.com/myclms/pictures/raw/master/20240403161422.png)

### 3) Sarse ( for action value )

state-action-reward-state-action

**algorithm**:

![](https://gitee.com/myclms/pictures/raw/master/20240403161829.png)

+policy improvement:

![](https://gitee.com/myclms/pictures/raw/master/20240405102023.png)

### 4) Expected Sarsa and n-step Sarsa

**Expected Sarsa**:

algorithm and difference to Sarsa:

![](https://gitee.com/myclms/pictures/raw/master/20240405102959.png)

**n-step Sarsa**:

relationship among Sarsa,n-step Sarsa and MC:

![](https://gitee.com/myclms/pictures/raw/master/20240405104059.png)

![](https://gitee.com/myclms/pictures/raw/master/20240405104446.png)

**A compromise for Sarsa and MC.**

* Sarsa:t(online)
* n-step Sarsa:t+n
* MC:until the episode's end(offline)

So,

![](https://gitee.com/myclms/pictures/raw/master/20240405105132.png)

### 5) Q-learning(optimal action values)

optimal action values:no need for swiching between "policy evaluation" and "policy improvement".

**algorithm**:

![](https://gitee.com/myclms/pictures/raw/master/20240405105631.png)

**It solves the BOE!**

some added concepts:

![](https://gitee.com/myclms/pictures/raw/master/20240405110236.png)

Sarsa is on-policy(policy is improved constantly).Q-learning can be off-policy.

**pseudocode**:

on-policy version:

![](https://gitee.com/myclms/pictures/raw/master/20240405111650.png)

**$\epsilon$-Greedy** 

**off-policy version**:

![](https://gitee.com/myclms/pictures/raw/master/20240405112005.png)

$\pi_b$ can have higher <u>exploring quality</u>.

$\pi_T$ can be **Greedy**!Because <u>target-policy isn't used to generate data afterwards</u>.

### 6) Summary

**a unified expression** for TD algorithm introduced above:

![](https://gitee.com/myclms/pictures/raw/master/20240405113227.png)

The difference is just the TD target.

![](https://gitee.com/myclms/pictures/raw/master/20240405113427.png)

![](https://gitee.com/myclms/pictures/raw/master/20240405113551.png)

## 8.Value Function Approximation

Tabular state-value/action-value is transformed into a function.

**The task**:find best $w$ to fit $v_\pi\ or\ q_\pi$ for policy evaluation.

why need curve fitting?

![](https://gitee.com/myclms/pictures/raw/master/20240406124533.png)

less storage and more generalization , but less accuracy.

![](https://gitee.com/myclms/pictures/raw/master/20240406135108.png)

![](https://gitee.com/myclms/pictures/raw/master/20240407142526.png)



### 1) Objective function

**for state value**:

objective function:

![](https://gitee.com/myclms/pictures/raw/master/20240406141002.png)

steady-state and long-run distribution:

![](https://gitee.com/myclms/pictures/raw/master/20240406140152.png)

The weight $d_\pi (s)$ indicates the importance of one state.

### 2)Objective Optimization function

The GD->SGD and MC/TD (for approximation of  $v_\pi$ ) algorithm is used to solve the minimization question:

![](https://gitee.com/myclms/pictures/raw/master/20240407141024.png)

![](https://gitee.com/myclms/pictures/raw/master/20240407141230.png)

![](https://gitee.com/myclms/pictures/raw/master/20240407141429.png)

![](https://gitee.com/myclms/pictures/raw/master/20240407141526.png)

**How to select the from of $\hat{v}$ ?**

![](https://gitee.com/myclms/pictures/raw/master/20240407142021.png)

Linear function is easier to be used for study and tabular is a special form of it. 

**TD-Linear-Objective Function**

the solved true problem:

![](https://gitee.com/myclms/pictures/raw/master/20240407144926.png)

### 3) Sarsa and Q-learning with function approximation

**Sarsa**:

**for action value**:

![](https://gitee.com/myclms/pictures/raw/master/20240407145144.png)

pseudocode:

![](https://gitee.com/myclms/pictures/raw/master/20240407145603.png)

**Q-learning**:

**for optimal action value**:

![](https://gitee.com/myclms/pictures/raw/master/20240407145836.png)

pseudocode:

![](https://gitee.com/myclms/pictures/raw/master/20240407150238.png)

### 4) Deep Q-learning

also called **Deep Q-network(DQN)**.Introduce **Neural Network**.

**Objective function**:

![](https://gitee.com/myclms/pictures/raw/master/20240407151013.png)



**First technique**:Two networks

GD is difficult because $w$ appeared twice makes it's difficult to <u>caculate the gradient</u> , but we can <u>simplify the problem</u>:

![](https://gitee.com/myclms/pictures/raw/master/20240407151414.png)

![](https://gitee.com/myclms/pictures/raw/master/20240407152158.png)

**Another technique**:Experience replay

definition:

![](https://gitee.com/myclms/pictures/raw/master/20240407152542.png)

why needed?

Avoid the waste of gotten experience;

Value Function Approximation introduces $E$ (expectation).So,we need give the variables a distribution( often as uniform ) unless we use stochastic method to delete the $E$ .



Due to something( NN can fit the truth better and easily ),we use <u>Neural Network</u> instead of using the basic GD algorithm to solve the minimization problem .

**Pseudocode**:

![](https://gitee.com/myclms/pictures/raw/master/20240407155834.png)

## 9. Policy Gradient Methods

path:determine metrics -> optimize metrics

Tabular policy is transformed into a function.<u>The benifits are same as those that introduced in Part 8.</u> 

New definition of optimal policy:

![](https://gitee.com/myclms/pictures/raw/master/20240407171704.png)

**Basic idea** or Summary:

![](https://gitee.com/myclms/pictures/raw/master/20240407172052.png)

### 1) Metrics

two functions of $\theta$ .

#### average state value

![](https://gitee.com/myclms/pictures/raw/master/20240407181519.png)

determine $d(s)$ :

1. independent

   ![](https://gitee.com/myclms/pictures/raw/master/20240407181656.png)

2. dependent

   stationary distribution as introduced in Part 8-Objectove function.

#### average one-step reward

![](https://gitee.com/myclms/pictures/raw/master/20240407182304.png)

relationship:

![](https://gitee.com/myclms/pictures/raw/master/20240407183007.png)

### 2) Gradient of metrics

![](https://gitee.com/myclms/pictures/raw/master/20240407183943.png)

NN help we do this:

![](https://gitee.com/myclms/pictures/raw/master/20240407184637.png)

![](https://gitee.com/myclms/pictures/raw/master/20240407184809.png)

![](https://gitee.com/myclms/pictures/raw/master/20240407184924.png)

### 3) Gradient-ascent algorithm(REINFORCE)

**algorithm**:

true value -> approximation

![](https://gitee.com/myclms/pictures/raw/master/20240407185145.png)

![](https://gitee.com/myclms/pictures/raw/master/20240407185322.png)

**interpretation**:

$\frac{d\ lnf(x)}{d\ x} = \frac{f^{\prime}(x)}{f(x)}$ 

![](https://gitee.com/myclms/pictures/raw/master/20240407190544.png)

![](https://gitee.com/myclms/pictures/raw/master/20240407190427.png)

So,it keeps a good balance between exploration and exploition.

pseudocode:

on-policy,off-line(MC)

![](https://gitee.com/myclms/pictures/raw/master/20240407191753.png)

## 10. Actor-Critic

8+9+7

### 1) QAC

![](https://gitee.com/myclms/pictures/raw/master/20240409210522.png)

### 2) A2C

introduce **baseline** to reduce the variance of samples ( no influence to expectation ).

![](https://gitee.com/myclms/pictures/raw/master/20240409211448.png)

![](https://gitee.com/myclms/pictures/raw/master/20240410185813.png)

![](https://gitee.com/myclms/pictures/raw/master/20240410190104.png)

pseudocode:

![](https://gitee.com/myclms/pictures/raw/master/20240410190135.png)

### 3) Off-policy actor-critic

**Importance sampling**:problem like :E[*],on-policy -> off-policy.

MC:

![](https://gitee.com/myclms/pictures/raw/master/20240410190837.png)

Importance sampling:

why : to estimate $E_{x \sim p_0}(x)$ according to samples from $p_1$ 

![](https://gitee.com/myclms/pictures/raw/master/20240410191506.png)

![](https://gitee.com/myclms/pictures/raw/master/20240410191757.png)

![](https://gitee.com/myclms/pictures/raw/master/20240410192827.png)

algorithm:

![](https://gitee.com/myclms/pictures/raw/master/20240410193016.png)

pseudocode:

![](https://gitee.com/myclms/pictures/raw/master/20240410193135.png)

###  4) Deterministic Actor-Critic(DPG)

In the algorithm introduced above , $\pi(a|s,\theta) > 0$ for every $(s,a)$ .(disadvantage: require the number of actions is limited )

new representation for policy:

![](https://gitee.com/myclms/pictures/raw/master/20240410194436.png)

pseudocode:

![](https://gitee.com/myclms/pictures/raw/master/20240410195133.png)

remarks:

![](https://gitee.com/myclms/pictures/raw/master/20240410195609.png)



