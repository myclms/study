# 我的爬虫脚本仓库

[gitee](https://gitee.com/myclms/study_crawler)

# 前置概念

## 请求过程

1. 网络请求

   ```mermaid
   graph LR
   A[客户端]-->|url|B[DNS服务器]
   B-->|ip|A
   A-->|请求|C[服务器]
   C-->|响应|A
   ```

   一个请求对应一个数据包（html,css,js,jpg,...）

   * http协议(超文本传输协议，默认端口号80)
   * https协议(http+ssl(安全套接字层，对传输内容加密),443)

   浏览器发送请求:

   1. 构建请求
   2. 查找缓存
   3. 拦截请求，返回缓存中的资源/准备ip地址和端口
   4. 等待TCP队列
   5. 建立TCP连接
   6. 发送HTTP请求

2. 请求过程

3. 请求组成

   * 请求网址 url

   * 方法 methods:get,post

   * 头 header

     ==User-Agent,cookie,referer==

   * 体 body

robots协议

异步加载数据包：向网站进行一次请求，一次只传部分数据

## 爬虫概念

概念：模拟浏览器（客户端），发送请求，获取响应。

特点：知识碎片化；网站变化多。

* 反爬
* 反反爬
  1. 脚本伪装成客户端：

     1. 构建User-Agent池（列表），随机使用--random.choice(UAlist)/调用库--from fake_useragent import UserAgent,UserAgent().random.

     2. headers带User-Agent

  2. 模拟登录：

     1. cookie

     2. requests.session()--实例化session对象

        session.get()/post()

  3. 

## 作用

1. 数据采集
2. 软件测试
3. 抢票
4. 网络安全
5. web漏洞扫描
6. ……

## 分类

根据爬取网站的数据：通用爬虫（搜索引擎）； ==聚焦爬虫== 。

# 基本步骤

1. 确定url
2. 发送请求
3. 解析请求
4. 保存数据

# 头部信息确认

一般都要带上User-Agent,Referer.

失败后：排错--XHR校验:在请求的URL路径添加XHR断点（协议://域名/路径？参数）

![](https://gitee.com/myclms/pictures/raw/master/20240217101044.png)

XMLHttpRequest:



# 字符串编解码

* encode:string->bytes
* decode:bytes->string

# requests库

1. response = requests.get(url,headers,params,proxies,timeout),requests.post(url,data)

2. response.text,response.content,response.json()

   get: 

    * response.encoding = 'utf-8'

      response.text

   	* response.content.decode()

   save:

   ​	 with open('1.txt','w',encoding='utf-8')

3. res.url,res.status_code,res.headers,res.request.headers

# URL传参

1. 发送请求时，参数由明文转换成密文(如：百度搜索‘学习‘->'%xx%xx...')

   相关库：from urllib.parse import quote,unquote

   url = www.baidu.com/s?wd=学习

2. 请求参数字典params

# 出错后重复加载多次和超时停止

刷新：

```python
from retrying import retry

@retry(stop_max_attempt_number=3)

def test():
	res = requests.get(url)
    
try:
    test()#尝试3次
except:
    print('...')#3次都失败

```

# 文本解析

## 文本分类

* 结构化
  1. json
  2. xml

* 非结构化
  1. html

## json

dict->json str:json.dumps(dict,ensure_ascii=false)

json->dict:json.loads(...)

### jsonpath模块

[python-jsonpath模块教程](https://blog.csdn.net/nd211314555/article/details/88426529)

##xml、html

xpath helper!!!

[XPath定位语法总结](https://blog.csdn.net/Q0717168/article/details/118819843)

### lxml模块

文本：a/text()

属性：a/@href

## 正则

re模块

小坑：匹配表达式有括号要用转义符,即\\(

[正则表达式 – 语法 | 菜鸟教程](https://www.runoob.com/regexp/regexp-syntax.html)

# 多线程、进程

```python
from threading import Thread,current_thread

def test_func(x,y):
    print('线程名称：',current_thread().name)
    print(x+y)

t1 = Thread(target=test_func,args=(1,2))
t1.start()
```

还可以用装饰器（更方便快捷）

# mysql

* 关系型数据库：行和列的形式存储数据，易于维护，使用方便
* 非关系型数据库：单条数据的形式存储，格式灵活

……待补

# selenium+webdriver

情景：==elements（源代码文件编译执行后的）和源代码文件不同==(分析较复杂。js动态渲染)；自动化；模拟浏览器；……(==速度较慢==;但是较方便,==对新手友好==)

### 基本用法-获取源代码/模拟真人操作

```python
from selenium import webdriver
driver = webdriver.Edge()
driver.get(url)

driver.title
driver.page_source#浏览器！！！渲染后！！！的源代码
driver.current_url

driver.save_screenshot('file_name')
driver.quit()
```

**绕过一些检测**：

```python
option = webdriver.EdgeOptions()

option.add_experimental_option('excludeSwitches',['enable-automation'])

driver = webdriver.Edge(options=option)
```



**开启无头模式**：

```python
option = webdriver.EdgeOptions()

option.add_argument('--headless')

driver = webdriver.Edge(options=option)

……
```

**暂停**：

```python
import time

time.sleep(seconds)
```

**定位元素+元素操作**：

[定位元素](https://blog.csdn.net/qq_16519957/article/details/128740502)

注：CLASS_NAME定位，CLASS_NAME含空格，空格要换成.

[元素对象操作](https://blog.csdn.net/minzhung/article/details/102690970)

```python
ele = driver.find_element_by_id('...') -> WebElement

ele.send_keys('...')
ele.click()
ele.submit()
ele.clear()# 清除文本值

ele.size -> tuple
ele.text -> str
ele.get_attribute('name') -> str
ele.is_displayed() -> True/False# 元素是否可见
```

**标签页切换（执行JS代码）**：

```python
# 通过js脚本新开标签页
js = 'window.open("https://www.sougou.com")'
driver.execute_script(js)# 执行JS脚本
time.sleep(2)
# 获取当前所有标签页
windows = driver.window_handles
# 跳转第一页
driver.switch_to.window(windows[0])
```

**页面滚动（执行JS代码）**：

```python
js = 'document.documentElement.scrollTop = 8000'#滚动到底部
js = 'window.scrollTo(0,700)'#滚动到（0,700）像素位置

driver.execute_script(js)
```

**其他常用操作**：

```python
# 设置弹窗大小
driver.set_window_size(480,800)
# 后退、前进
driver.back()
driver.forward()
# 刷新页面
driver.refresh()
# 鼠标ActionChains类，键盘Keys类（宏），元素显（等到某一特定元素定位到）/隐（等到元素定位到）等待，多表单切换，下拉框Select类
```

## mitmproxy

[Introduction (mitmproxy.org)](https://docs.mitmproxy.org/stable/)

[(mitmproxy -- mitmdump: 从安装到简单使用)](https://www.bilibili.com/video/BV1UC4y1t7EL?p=3&vd_source=858893c79cd58f7ceaa6831fb2deaa57)

### DrissionPage

[🛸 模式切换 | DrissionPage (gitee.io)](https://g1879.gitee.io/drissionpagedocs/WebPage/mode_change)

[🔦 基本用法 | DrissionPage (gitee.io)](https://g1879.gitee.io/drissionpagedocs/get_elements/usage)

# Scrapy框架

...

# BeautifulSoup

# 常见的反爬手段

1. 判断请求头 -- 加上请求头
   * user-agent	用户代理(搜集User-Agent列表，每次请求随机使用)
   * refer    请求来源
   * cookie    访问凭证
2. 验证码
3. ip -- ip代理
4. 请求频率 -- time.sleep()
5. 自定义字体，css、js等
6. 爬虫访问到假数据 -- 分析网页
7. 动态渲染（XHR,源码中没有看到的数据）--抓包分析；动态渲染工具（selenium）

## Cookie反爬

* 普通：携带上Cookie即可
* 高难度：时效性；动态生成的参数

## 头部Sign

逆向加密算法（常含md5算法）

## IP限制

* 正向代理和反向代理
* （知道使用了代理IP，也知道真实IP）透明代理、匿名代理、高匿代理

requests.get(url,proxies)

构建代理IP池

## 字体混淆

案例：起点月票榜（1）

1. 发随机字体文件 -- 获取字体文件，自制动态的映射表

   ```python
   # from io import BytesIO
   # from fontTools.ttLib import TTFont
   
   font_file = TTFont(BytesIO(font_file_res.content))#（临时数据）不用保存为磁盘文件，保存在内存里的文件
       cmap = font_file.getBestCmap()
       font_file.close()
       # print(cmap)
       my_map = {
           'zero': '0',
           'one': '1',
           'two': '2',
           'three': '3',
           'four': '4',
           'five': '5',
           'six': '6',
           'seven': '7',
           'eight': '8',
           'nine': '9',
       }
       plain_num = ''
       for num in data:
           plain_num += my_map[cmap[int(num)]]
   ```

   

2. 固定的映射表 -- 自制固定的映射表

   类似于上面的‘my_map’

3. CSS映射（偏移）-- 找规律，复刻偏移数据

4. 图片伪装 -- 下载图片(实在不行就selenium,不寒碜)+OCR识别（ddddocr）



## 特征识别



 

## 验证码(全自动区分计算机和人的图灵测试)

类型：

1. 数字、字母

2. 算数

3. 滑块

4. 汉字

5. 图片

6. ###### ……

使用场景：注册；登录；频繁请求

**应对策略**：

1. 登录：可以手动登录，然后用Cookie模拟登录。
2. OCR/打码平台
3. selenium模拟滑块滑动



# ！！！js逆向！！！

1. js实现跳转 -- 开发者工具
2. js生成请求参数 -- 分析对应js代码
3. js实现数据**加密和加盐**

## 加密

加密算法：md5(不可逆，固定长度),base64(可逆，不固定长度)，...

**hashlib模块**

```python
import hashlib

pt = '哈哈'

# 方法，加密二进制内容
res = hashlib.new('md5',pt.encode())
# res = hashlib.md5(pt.encode())

print(res.hexdigest())
```

**base64模块**

```python
import base64

pt = '12345'
# 加密
res = base64.b64encode(pt.encode()).decode()
print(res)
# 解密
res1 = base64.b64decode(res.encode()).decode()
print(res1)
```

## 小纲

1. 加密参数、数据的定位及解密（抓包，看源代码，==搜索关键字==）
2. HOOK关键字定位脚本（Ctrl+enter执行）；本地覆盖（JS文件保存到本地，用浏览器执行修改后的JS文件，以测试）；补浏览器环境（jsdom第三方库）



# 网页爬取案例

## 1.B站 19金先生 投稿视频 下载

后记：

1. 未成功在用户空间中抓取（动态页面,selenium也许可行），去全部播放列表寻求出路成功
2. 练习知识点：正则，音视频合并，网络请求，==抓包分析==

## 2.有道、百度翻译

js逻辑分析与实现

sign获取失败。。。

## 3.[起点月票](https://www.qidian.com/rank/yuepiao/)

1. 字体加密：字体文件获取，cmap获取，数据解码
2. 正则匹配（xpath失败？）
3. from io import BytesIo：内存文件

## 4.[烯牛数据](https://www.xiniudata.com/industry/newest?from=data)

==加密参数、加密数据==

负载：payload,sig在变 -- 参数加密
发起程序 可读性高 -- 无混淆JS
**参数解密：**

1. 搜索参数/点击发起程序搜索,断点可能的地方，刷新，进入Debug就是要找的代码

   ![](https://gitee.com/myclms/pictures/raw/master/20240217105729.png)

2. 加密参数是谁？可以控制台输出

3. JS文件：补参数生成的JS逻辑代码：补函数，补变量（重复控制台输出，结果相同则为常量）

4. [Python执行JS代码的三种方式-CSDN博客](https://blog.csdn.net/qq_40734108/article/details/110757610)

**数据解密**：

1. 'd'太泛 -- 搜索JSON.parse( ,decrypt(,url路径等-- 数据类型转换(后端数据渲染到前端)
2. 格式：JSON.parse(解密方法(密文))
3. 测试找到的接口（控制台输出）是不是要找的数据

## 5.[交易列表 - 福建省公共资源交易电子公共服务平台](https://ggzyfw.fj.gov.cn/business/list/)

头部签名加密(portal-sign)、ts(time.time())、数据加密(AES)

终端执行：npm install crypto-js --save，配置js解密环境

const CryptoJS require('crypto-js')

## 6.[同花顺问财 (iwencai.com)](https://www.iwencai.com/unifiedwap/result?w=AI&querytype=stock)

[js逆向中常用hook脚本合集 - 掘金 (juejin.cn)](https://juejin.cn/post/7207406497508589625)

本地覆盖



# 工具

1. [简单爬虫代码在线生成（复制数据包cURL）](https://curlconverter.com/)
2. 插件：xpath helper(生成、验证xpath路径)





[https://www.bilibili.com/video/BV1UC4y1t7EL?p=3&vd_source=858893c79cd58f7ceaa6831fb2deaa57]: 