import numpy as np

# 输入判断矩阵
A = np.array([[1,2,3,5],[1/2,1,1/2,2],[1/3,2,1,2],[1/5,1/2,1/2,1]])
n = A.shape[0]

# 算数平均值法求权重
# 按列求和
A_c = np.sum(A,axis=0)
# 归一化
Stand_A = A/A_c
# 对归一化矩阵按行求和
A_r = np.sum(Stand_A,axis=1)
# 计算权重向量
weights = A_r/n

print(weights)

# 几何平均法
# 按行求积
A_r = np.prod(A,axis=1)
A_r = np.power(A_r,1/n)
# 归一化
Stand_A = A_r/np.sum(A_r)
weights = Stand_A

print(weights)

# 特征值法
# 求最大特征值和特征向量
eig_val,eig_vec = np.linalg.eig(A)
max_eig_val_id = np.argmax(eig_val)
max_eig_vec = eig_vec[:,max_eig_val_id]
# 归一化
weights = max_eig_vec/np.sum(max_eig_vec)

print(weights)
