import numpy as np

# 输入n*m矩阵
n = int(input("请输入参评方案数目："))
m = int(input("请输入指标数目："))
kind = input("请输入各指标类型：\n1.极大型 2.极小型 3.中间型 4.区间型\n").split(" ")

A = np.zeros(shape=(n, m))
print("请输入数据矩阵")
for i in range(n):
    A[i] = input().split(" ")
    A[i] = list(map(float, A[i]))
print("数据矩阵为：\n{}".format(A))


# 数据矩阵指标类型转换成极大型
#
def min_to_max(i):
    x = np.max(A[:, i])
    xs = list(A[:, i])
    xs = [x-e for e in xs]
    A[:, i] = np.array(xs)


def mid_to_max(i):
    print("指标%d，请输入最佳值：" % (i+1))
    x_best = float(input())
    xs = list(A[:, i])
    xs = [abs(e - x_best) for e in xs]
    x = np.max(xs)
    if x == 0:
        x = 1
    xs = [1-e/x for e in xs]
    A[:,i] = np.array(xs)


def ran_to_max(i):
    print("指标%d，请输入区间值[a,b]：" % (i+1))
    a = float(input("a="))
    b = float(input("b="))
    x_min = np.min(A[:,i])
    x_max = np.max(A[:,i])
    x = max(x_max-b, a-x_min)
    xs = list(A[:,i])
    for j in range(n):
        if xs[j] < a:
            xs[j] = 1 - (a - xs[j]) / x
        elif xs[j] <= b:
            xs[j] = 1
        else:
            xs[j] = 1 - (xs[j] - b) / x
    A[:,i] = np.array(xs)


def transform():
    for i in range(m):
        if kind[i] == 'analyze':
            pass
        elif kind[i] == '2':
            min_to_max(i)
        elif kind[i] == '3':
            mid_to_max(i)
        else:
            ran_to_max(i)
#
transform()
print("转化成极大型指标后的数据矩阵：\n{}".format(A))

# 标准化处理，各项指标加上权重（目前假设相等）
A = A.astype('float')
for i in range(m):
    A[:, i] = A[:, i]/np.sqrt(np.sum(A[:, i]**2))
# 加上权重
print("标准化矩阵为：{}".format(A))

# d+,d-
x_max = np.max(A,axis=0)
x_min = np.min(A,axis=0)
d_z = np.empty((n,1))
d_f = np.empty((n,1))
for i in range(n):
    d_z[i,:] = np.sqrt( np.sum(np.square(A[i, :] - x_max) ) )
    d_f[i,:] = np.sqrt( np.sum(np.square(A[i, :] - x_min) ) )
print("x_max:\n",x_max)
print("x_min:\n",x_min)
print("d_z:\n",d_z)
print("d_f:\n",d_f)

# 计算各方案得分
s = d_f/(d_z+d_f)
# 百分制
score = 100*s/sum(s)
for i in range(n):
    print(f"第{i+1}个方案得分为：{score[i]}")