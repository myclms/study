import numpy as np

# 输入判断矩阵
A = np.array([[1,2,3,5],[1/2,1,1/2,2],[1/3,2,1,2],[1/5,1/2,1/2,1]])
n = A.shape[0]

# 求特征值和特征向量等值
eig_val,eig_vec = np.linalg.eig(A)
max_eig_val = max(eig_val)
# 一致性检验
CI = (max_eig_val-n)/(n-1)
RI = [0,0.0001,0.52,0.89,1.12,1.26,1.36,1.41,1.49,1.52,1.54,1.56,1.58,1.59]
CR = CI/RI[n-1]

# 输出一致性检验结果
print("CI=",CI)
print("CR=",CR)

if CR < 0.10:
    print("判断矩阵一致性可以接受")
else:
    print("判断矩阵一致性不可以接受")