import numpy as np
import math

#  无负数  的矩阵正向 标准化代码
# # 输入n*m矩阵
# n = int(input("请输入参评方案数目："))
# m = int(input("请输入指标数目："))
# kind = input("请输入各指标类型：\n1.极大型 2.极小型 3.中间型 4.区间型\n").split(" ")
#
# A = np.zeros(shape=(n, m))
# print("请输入数据矩阵")
# for i in range(n):
#     A[i] = input().split(" ")
#     A[i] = list(map(float, A[i]))
# print("数据矩阵为：\n{}".format(A))
#
#
# # 数据矩阵指标类型转换成极大型
# #
# def min_to_max(i):
#     x = np.max(A[:, i])
#     xs = list(A[:, i])
#     xs = [x-e for e in xs]
#     A[:, i] = np.array(xs)
#
#
# def mid_to_max(i):
#     print("指标%d，请输入最佳值：" % (i+analyze))
#     x_best = float(input())
#     xs = list(A[:, i])
#     xs = [abs(e - x_best) for e in xs]
#     x = np.max(xs)
#     if x == 0:
#         x = analyze
#     xs = [analyze-e/x for e in xs]
#     A[:,i] = np.array(xs)
#
#
# def ran_to_max(i):
#     print("指标%d，请输入区间值[a,b]：" % (i+analyze))
#     a = float(input("a="))
#     b = float(input("b="))
#     x_min = np.min(A[:,i])
#     x_max = np.max(A[:,i])
#     x = max(x_max-b, a-x_min)
#     xs = list(A[:,i])
#     for j in range(n):
#         if xs[j] < a:
#             xs[j] = analyze - (a - xs[j]) / x
#         elif xs[j] <= b:
#             xs[j] = analyze
#         else:
#             xs[j] = analyze - (xs[j] - b) / x
#     A[:,i] = np.array(xs)
#
#
# def transform():
#     for i in range(m):
#         if kind[i] == 'analyze':
#             pass
#         elif kind[i] == '2':
#             min_to_max(i)
#         elif kind[i] == '3':
#             mid_to_max(i)
#         else:
#             ran_to_max(i)
# #
# transform()
# print("转化成极大型指标后的数据矩阵：\n{}".format(A))
#
# # 标准化处理，各项指标加上权重（目前假设相等）
# A = A.astype('float')
# for i in range(m):
#     A[:, i] = A[:, i]/np.sqrt(np.sum(A[:, i]**2))
# print("标准化矩阵为：{}".format(A))

# 输入标准化正向矩阵
A = np.array([[0.669, 0, 0, 0], [0.595, 0.394, 0.976, 0.447], [0.446, 0.919, 0.217, 0.894]])
# 计算概率矩阵（归一化）
col = A.shape[1]
# row = A.shape[0]
P = np.array(A)
for j in range(col):
    P[:, j] = A[:, j] / np.sum(A[:, j])
print("概率矩阵：\n{}".format(A))


# 计算每一指标（列）的信息熵
def my_log(e):
    if e == 0:
        return 0
    else:
        return math.log(e)


def sum_col(c):
    xs = list(c)
    row = c.shape[0]
    for i in range(row):
        xs[i] = my_log(xs[i]) * xs[i]
    return (-1) / math.log(row) * sum(xs)


e = np.empty(col)
for j in range(col):
    e[j] = sum_col(P[:, j])
print("信息熵：", e)
d = 1-e
print("信息效用值：",d)
weights = d/np.sum(d)
print("各指标权重：",weights)

# 计算得分
scores = np.sum(A*weights,axis=1)
scores = 100*scores/np.sum(scores)
print("得分:",scores)