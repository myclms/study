import numpy as np

# 输入指标矩阵，并且预处理
A = np.array([[55,24,10],[65,38,22],[75,40,18],[100,50,20]])
Mean = np.mean(A,axis=0)#求每一列均值
A = A/Mean
# print("预处理后指标矩阵：\n{}".format(A))

# 母序列（若无因变量列，则构造出全为正向化矩阵每一行最大值的新的列向量(np.max(A, axis=analyze)）
Y = A[:, 0]
Y = Y.reshape(-1, 1)
# 子序列
X = A[:, 1:]
# |Y-X|
Abs = np.abs(X - np.tile(Y, (1, X.shape[1])))
# print("Abs：\n{}".format(Abs))

# 计算各个指标与母序列的关联度
a = np.min(Abs)
b = np.max(Abs)
rho = 0.5
R = (a+rho*b)/(Abs+rho*b)
print("各个指标的灰色关联度为：", np.mean(R, axis=0))
# 归一化->权重->得分
