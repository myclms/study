import numpy as np
import matplotlib.pyplot as plt


# 绘制3D球
def pltglobe(a=0, b=0, c=0, r=1, dense=1000):
    """
    :param a: 球心x坐标
    :param b: 球心y坐标
    :param c: 球心z坐标
    :param r: 半径
    :param dense: 绘制精度
    :return:
    """
    m = np.linspace(0, 2 * np.pi, dense)
    n = np.linspace(0, np.pi, dense)
    m,n = np.meshgrid(m,n)

    x = a + r * np.cos(m) * np.sin(n)
    y = b + r * np.sin(m) * np.sin(n)
    z = c + r * np.cos(n)

    ax = plt.axes(projection='3d')
    ax.set_xlabel('X')
    ax.set_ylabel('Y')
    ax.set_zlabel('Z')
    label = '({},{},{})\nr={}'.format(a, b, c, r)

    ax.plot_surface(x, y, z, color='r', label=label, alpha=0.2)
    plt.legend()
    plt.show()


pltglobe()
