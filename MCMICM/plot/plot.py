from matplotlib.pyplot import *
import numpy as np

# 0~2pi ,sinx,cosx^2
x = np.linspace(0, 2*np.pi, 200)
y1 = np.sin(x)
y2 = np.cos(np.power(x, 2))
y3 = np.cos(x)

# ylabel('$y$', rotation=90)
ax1 = subplot(2,2,1)
ax1.plot(x, y1, label='$sin(x)$')
legend()

ax2 = subplot(2,2,2)
ax2.plot(x, y2, ':', label='$cos(x^2)$')
legend()

ax3 = subplot(2,1,2)
ax3.plot(x, y3, 'y--', label='$cos(x)$')
legend()

show()