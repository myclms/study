import numpy as np
import pandas as pd
from matplotlib.pyplot import *

# 观测值输入
y = np.array([4.81, 4.8, 4.73, 4.7, 4.7, 4.73, 4.75, 4.75, 5.43, 5.78, 5.85])


def expmove(y, a):
    """
    指数平滑移动
    :param y: 观测值数组
    :param a: 权重
    :return: yhat
    """
    n = len(y)
    yhat = np.empty(n)
    yhat[0] = np.sum(y[0:3]) / 3
    for i in range(1, n):
        yhat[i] = a * y[i - 1] + (1 - a) * yhat[i - 1]
    return yhat


a = 0.8  # 权重
yhat = expmove(y, 0.8)
s = np.sqrt(((yhat - y) ** 2).mean())
res = a * y[-1] + (1 - a) * yhat[-1]
print("标准误差为：", s)
print("预测值为：", res)

# 画图
x = np.arange(0, len(y), 1)
scatter(x, y, color='b', label='y')
scatter(x, yhat, color='y', label='yhat1')
legend()
show()
#
# # 2次
# yhat = expmove(yhat,a)
# scatter(x, yhat, color='r', label='yhat2')
# legend()
# show()
