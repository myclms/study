import numpy as np
from matplotlib.pyplot import *

# 数据录入
x = np.arange(0, 1.1, 0.1)
y = np.array([-0.447, 1.978, 3.28, 6.16, 7.08, 7.34, 7.66, 9.56, 9.48, 9.30, 11.2])

# 散点图绘制
# scatter(x, y)
# show()

# 拟合
deg = 2
f = np.polyfit(x, y, deg=deg)  # deg次多项式
fname = ''
for i in range(deg+1):
    if i < deg:
        fname = fname+str(f[i])+'x^'+str(deg-i)+'+'
    elif deg-i == 1:
        fname = fname + str(f[i]) + 'x'+'+'
    else:
        fname = fname+str(f[i])
print("{}阶多项式拟合函数为：".format(deg), fname)

# 拟合值和原数据值同时绘制图
yhat = np.polyval(f, x)
plot(x, y, '-', color='b', label='$y$')
plot(x, yhat, '-', color='y', label='$yhat$')
legend()
show()
