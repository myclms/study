import numpy as np
from scipy.optimize import curve_fit
# from matplotlib.pyplot import *

# 数据录入
x = np.arange(0, 1.1, 0.1)
y = np.array([-0.447, 1.978, 3.28, 6.16, 7.08, 7.34, 7.66, 9.56, 9.48, 9.30, 11.2])

# 散点图绘制
# scatter(x, y)
# show()

# 拟合
f = lambda x,a,b,c : a*x**2+b*x+c
# popt = curve_fit(f, x, y, bounds=[0, [3., 1., 0.5]])
popt = curve_fit(f, x, y)
print(popt[0])
