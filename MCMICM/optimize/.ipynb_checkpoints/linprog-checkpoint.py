from scipy.optimize import linprog
from numpy import ones,diag,c_,zeros
import matplotlib.pyplot as plt

rr = []
ss = []
r = 0
c = [-0.05,-0.27,-0.19,-0.185,-0.185]
a = c_[zeros(4),diag([0.025,0.015,0.055,0.026])]
# b = ones(4)*x
aeq = [[1,1.01,1.02,1.045,1.065]]
beq = [1]

bl = [0]*len(c)
br = [None]*len(c)
bounds = tuple(zip(bl,br))

while r<0.05:
    b = ones(4) * r
    res = linprog(c,a,b,aeq,beq,bounds,method='highs')
    rr.append(r),ss.append(-res.fun)
    x = res.x
    r = r+0.001

print("optimized x:",x)
print("optimized value of F:",res.fun)

# plt.plot(rr,ss,'r*')#'r*'红色的*
# plt.xlabel('$r$'),plt.ylabel('$Q$')#$$斜体
# plt.savefig('practice1_1.png',dpi=500)#dpi=500大小
# plt.show()


