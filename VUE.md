# VUE3

组合式API（composition API，区别于2的配置项式API -- options API）

创建vue3工程(基于vite,需要具备node环境)：

```bash
npm create vue@latest
...根据提示确定具体配置
...根据提示安装项目依赖模块
```

启动工程

```bash
npm run dev
```



主要是src文件夹

## 1. 基本模式(setup 语法糖)

```vue
<template lang="html">
    ...html
</template>
<script lang="ts" >
    export default {
        name: '组件名',
    }
</script>
<script lang="ts" setup >
    ...ts
    // 数据，方法，...
</script>
<style lang="css" scoped >
    ...css
</style>
```

## 2. 响应式数据

（前端）页面和（前端）后台数据双向流动

```vue
import { ref, reactive } from 'vue'
```

ref主要用于基础数据,reactive主要用于对象数据（字典，列表等,对象整体改变要写Object.assign(stu, { name: 'cs' ,age:’xx’})）

1. ref属性用在html标签上：创建一个响应式数据（ref实例对象）基本类型，用来获取DOM元素（不使用id,避免和外部组件id冲突）；
2. ref属性用在组件标签上：需要内部组件主动使用**defineExpose**暴露数据或方法，外部组件才可以看到这些东西

```vue
let stu = reactive({name:'xxx',age:'xxx'})
let {name} = toRef(stu, 'name')
let {name, age} = toRefs(stu)
let title = ref(null)
```

## 3. 计算属性

连接一个值到另外的多个值

```vue
<script setup >
    import { ref, computed } from 'vue'
    let firstName = ref('zhang');
    let lastName = ref('san');
    let fullName = computed(() => {
        return firstName.value.slice(0,1).toUpperCase()+firstName.value.slice(1) + ' ' + lastName.value.slice(0,1).toUpperCase()+lastName.value.slice(1);
    }); //计算属性, 只读
    let fullName2 = computed({
        get(){
            return firstName.value.slice(0,1).toUpperCase()+firstName.value.slice(1) + ' ' + lastName.value.slice(0,1).toUpperCase()+lastName.value.slice(1);
        },
        set(value){
            let names = value.split(' ');
            this.firstName = names[0];
            this.lastName = names[1];
        }
    }); //计算属性, 可读可写
</script>
```

## 4. 监听

```vue
<script setup >
    import { ref, reactive, watch, watchEffect} from 'vue'
    // watch可以监视4种：ref,reactive,()=>{},[上述3种]
    const a = ref(1)
    const b = reactive({
        name: '张三',
        c: {
            c1: 'c1',
            c2: 'c2'
        },
    })

    function changeA() {
        a.value++
    }
    function changeBC() {
        let tc = b.c
        b.c = {
            c1: tc.c1+'~',
            c2: tc.c2+'~'
        }
    }
    function changeBC1() {
        b.c.c1 += '~'
    }
    function changeBName() {
        b.name += '~'
    }
    function changeB() {
        Object.assign(b, {
            name: '李四',
            c: {
                c1: 'c1',
                c2: 'c2'
            },
        })
    }

    let stopWatchA = watch(a, (newVal, oldVal) => {
        console.log('a变化了', newVal, oldVal)
        if (newVal > 5) {
            stopWatchA()// 停止监听
        }
    })
    // watch(b, (newVal, oldVal) => {
    //     console.log('b变化了', newVal, oldVal)
    // })// 自动深度监听（{ deep: true }）,新旧值都为最新的值
    // watch(() => b.name, (newVal, oldVal) => {
    //     console.log('b.name变化了', newVal, oldVal)
    // })
    // watch(() => b.c, (newVal, oldVal) => {
    //     console.log('b.c变化了', newVal, oldVal)
    // }, { deep: true })// 深度监听
    // watch(() => b.c.c1, (newVal, oldVal) => {
    //     console.log('b.c.c1变化了', newVal, oldVal)
    // })


    
    //watchEffect立即执行1次，并且会自动收集依赖，依赖变化会再次执行；但是无法获取oldVal
    // watchEffect(() => {
    //     console.log('watchEffect', a.value)
    // })
</script>
```



## 5. 父子组件通信

**defineProps,  defineExpose**

父组件：

``` vue
<template>
    <div class="app">
        <h1>hello, VUE!</h1>
        <Person ref="personRef" a="哈哈" :list="persons"/>
        <button @click="showLog">测试</button>
    </div>
</template>
<script lang="ts">
    export default {
        name: 'App',
    }
</script>
<script setup lang="ts">
    import Person from './components/Person.vue';
    import { ref, reactive } from 'vue';
    import { type Persons } from './types'
    let personRef = ref(null);
    function showLog() {
        console.log(personRef.value);// Person组件实例对象。只能看到组件内部使用defineExpose暴露的属性和方法
    }
    let persons = reactive<Persons>([
        { name: '张三', age: 18 },
        { name: '李四', age: 19 },
        { name: '王五', age: 20 },
    ])// reactive<Persons>([])泛型指定类型
</script>
```

子组件：

```vue
<template lang="html">
    <div class="person">
        <ul>
            <li v-for="item in list" :key="item.name">{{item.name}} - {{item.age}}</li>
        </ul>
    </div>
</template>
<script lang="ts">
    export default {
        name: 'Person',
    }
</script>
<script setup lang="ts">
    import { ref } from 'vue'
    import { type Persons } from '@/types'
    let x = ref(10)
    defineExpose({x})
    // defineProps(['list'])
    // defineProps<{list:Persons}>()
    // 接受list + 限制类型 + 限制必要性 + 默认值 
    withDefaults(defineProps<{list?:Persons}>(), {
        list: () => [{name: '张三', age: 18}]
    })
</script>
```



## 6. ts的好处

1. 泛型

2. 接口

   ```
   |-types
   	|-index.ts
   ```

   

3. 

## 7. 生命周期

创建+挂载，更新，卸载

一些钩子函数

```js
onBeforeMount(()=>{
    ...
})
onMounted((>{
    ...
})
update, unmount
```

自定义钩子（极致的模块化。实际上一个文件包含一个函数，函数提供数据和方法）

```
|-hooks
	|-useSum.ts	//所有和Sum相关的数据和方法 
	|-useDog.ts
```

```typescript
// useSum.ts
// import useSum from '@/hooks/useSum'
// let {sum,add} = useSum()
import { ref } from 'vue'

export default function () {
    let sum = ref(0)
    function add() {
        sum.value += 1
    }
    return {sum,add}
}
```

## 8.路由

安装： npm i vue-router

切换的路由组件，默认出现时挂载、消失时卸载

### 8.0. 规范

```
|-router
	|-index.ts	//1.创建路由器，编写路由规则
|-views			//路由组件
	|-xxx.vue
	|-xxx.vue
|-components	//普通组件
	|-xxx.vue
	|-xxx.vue
|-main.ts		//2.使用路由器
|-App.vue		//3.确定路由组件展示位置
```

```typescript
// router/index.ts
// 创建路由器并且暴露给main.ts

import { createRouter, createWebHistory } from 'vue-router'

import About from '@/views/About.vue'
import Home from '@/views/Home.vue'

const routes = [
  {
    path: '/home',
    name: 'Home',
    component: Home,
    children:[ // 嵌套路由
        {...}
    ]
  },
  {
    path: '/about',
    name: 'About',
    component: About
  },
  {
    path: '/',
	redirect: '/home'
  }//重定向
]

const router = createRouter({
  history: createWebHistory(),
  routes: routes
})

export default router
```



```vue
<template>
    <!-- 导航区 -->
    <div>
        <RouterLink to="/home" active-class="active">Home</RouterLink>
        
        <RouterLink to="/about" active-class="active">About</RouterLink>
        <RouterLink :to="{path:'/about'}" active-class="active">About</RouterLink>
        <RouterLink :to="{name:'About'}" active-class="active">About</RouterLink>
    </div>
    <!-- 展示区 -->
    <div>
        <RouterView />
    </div>
</template>
<script lang="ts">
    export default {
        name: 'App',
    }
</script>
<script setup lang="ts">
    import { RouterLink, RouterView} from 'vue-router';
</script>
<style>
    .active {
        ...
    }
</style>
```



两种工作模式：	一般 给客户用：history；后台管理：hash

![](https://gitee.com/myclms/pictures/raw/master/image-20250225111150503.png)

### 8.1. 传参

1. query

   传递：通过to属性

   ```html
   <RouterLink :to="'/about?a=${a}&b=&{b}'" active-class="active">About</RouterLink>
   
   <RouterLink
   	:to="{
   		path:'/about',
           query:{
           	a:a,
            	b:b
           }
       }"
   active-class="active">About</RouterLink>
   ```

   接收：useRoute

   ```javascript
   import {useRoute} from 'vue-router'
   let route = useRoute()
   console.log(route.query) // {a:...,b:...}
   ```

   

2. params

   需要在路由配置:

   ```typescript
   {
       path: '/about/:a/:b?',//问号表示b可不传
       name: 'about',
       component: About
     }
   ```

   

   传递：通过斜杠

   ```html
   <RouterLink :to="'/about/${a}/&{b}'" active-class="active">About</RouterLink>
   
   <RouterLink
   	:to="{
   		name:'about', // 只能用name
           params:{
           	a:a,
            	b:b
           }
       }"
   active-class="active">About</RouterLink>
   ```

   接收：useRoute

   ```javascript
   import {useRoute} from 'vue-router'
   let route = useRoute()
   console.log(route.params) // {a:...,b:...}
   ```

### 8.2. 路由的props配置

底层：<About a="..." b="..." />

1. 用于params参数

```typescript
{
    path: '/about/:a/:b?',//问号表示b可不传
    name: 'about',
    component: About,
    props: true
  }
```

此时使用：

```javascript
defineProps({'a','b'})
console.log(a)
console.log(b)
```

2. 都行

```javascript
{
    path: '/about/:a/:b?',//问号表示b可不传
    name: 'about',
    component: About,
    props(route){
        return route.query
    }
  }
```



### 8.3. 历史记录

回不去上一个记录

```html
<RouterLink replace :to="'/about/${a}/&{b}'" active-class="active">About</RouterLink>
```



### 8.4. 编程式路由

脱离<RouterLink/>

```javascript
import {useRouter} from 'vue-router'
const router = useRouter()
router.push('/path') //和to一样，也可字典
router.replace('/path')
```

@click=“functionName(params)” //可传参



## 9. 集中式状态（数据）管理工具

npm i pinia（管理组件之间共享的数据）

### 9.0. 模板

```
|-store
	|-组件名.ts
	|-组件名.ts
```



1. 搭载pinia环境

   ```typescript
   import App from './App.vue'
   
   import { createApp } from 'vue'
   import { createPinia } from 'pinia'
   
   const app = createApp(App)
   const pinia = createPinia()
   app.use(pinia)
   app.mount('#app')
   ```

   

2. 组件名.ts count.ts

   ```typescript
   import {defineStore} from 'pinia'
   export const useCountStore = defineStore('count',{
       state(){
           return {//真正的数据
               sum: 6
           }
       }
   })
   ```

   

3. 使用

   ```typescript
   import {useCountStore} from '@/store/count'
   let countStore = useCountStore()
   console.log(countStore.sum) // 6
   ```

   

4. 修改

   ```typescript
   //1
   //countStore.sum += 1
   //2 批量更改
   countStore.$patch({
       sum: 888,
       school:'xxx',
       ...
   })
   //3在store里加actions
   //count.ts
   actions:{
       functionName(params){
       	xxx
   	}
   }
   state(){...}
   ```

   

5. toRefs -> storeToRefs

6. getters(类似于计算属性)

   ```typescript
   actions:{...}
   state(){...}
   getters:{
       bigSum(state){
           return state.sum * 10
       }
   }
   ```

   ```typescript
   //组合式写法，类似于.vue里的script
   ```

   

7. $subscribe(类似于watchEffect)

   loveTalkStore.$subscribe((mutate, state)=>{

   ​	localStorage.setItem(‘talkList’, JSON.stringify(loveTalkStore.talkList))

   })

   // 使用时 

   // JSON.stringify(localStorage.getItem(‘talkList’) as string) || []

    

8. 







## 其他

1. npm i nanoid（随机生成唯一的字符串，或者uuid）

```javascript
import {nanoid} from ‘nanoid’

let id = nanoid()
```

2. npm i axios (网络请求)

```javascript
let {data:{xxx:otherName}} = await axios.get("url")
console.log(otherName)
```

3. 

