# tampermonkey

## 1. 基本构成

![](https://gitee.com/myclms/pictures/raw/master/image-20250110100616631.png)

1. 元数据
   - name		脚本名
   - version     版本管理
   - dedscription
   - author
   - icon
   - match        生效网址。可写多行match以对应多个网址。支持通配符“*”
   - grant          申请GM_*接口权限或者unsafeWindow,这是油猴的安全沙盒机制。
   - require       导入外部资源、库，比如jQuery.
   - run-at          脚本生效时间:document-start,document-end,cotext-menu
   - connect       声明跨域行为,允许脚本里的相应的跨域请求
2. 主要脚本内容

## 2. 一些知识，涉及JS

GM_*详见API[文档](https://bbs.tampermonkey.net.cn/thread-1909-1-1.html)

hook实例详见[教程](https://learn.scriptcat.org/%E6%B2%B9%E7%8C%B4%E6%95%99%E7%A8%8B/%E5%85%A5%E9%97%A8%E7%AF%87/hook%E5%AE%9E%E7%8E%B0b%E7%AB%99%E8%AF%84%E8%AE%BA%E5%B0%8F%E5%B0%BE%E5%B7%B4/)

调试与抓包[说明](https://learn.scriptcat.org/%E6%B2%B9%E7%8C%B4%E6%95%99%E7%A8%8B/%E5%85%A5%E9%97%A8%E7%AF%87/%E8%84%9A%E6%9C%AC%E5%9F%BA%E7%A1%80%E8%B0%83%E8%AF%95%E4%B8%8E%E6%8E%A7%E5%88%B6%E5%8F%B0%E4%BB%8B%E7%BB%8D/)





1. 有些时候，等待网页加载完成再执行脚本会解决Bug
2. 引入库要注意沙盒环境也许不兼容库，或者原网页已经引入过了
3. **函数劫持/hook函数, 提交/返回劫持**    一般来说，能 hook 的方法，应该满足他能访问到的对象，我们也能够访问得到，我们脚本能访问到的对象应该全在 `window`这个全局的变量上，所以上述例子都是以全局为基准的。    



1. GM_xmlhttpRequest **ajax请求**

2. MutationObserver **监听元素变化**

3. setInterval, setTimeout

4. GM_getValue, GM_setValue **存储值到油猴自带数据库（本地）**

   GM_deleteValue, GM_addValueChangeListener, GM_removeValueChangeListener, GM_listValues

5. if (weburl.indexOf(“xxx”) != -1) {xxx}    **判断网址是否包含“xxx”**

6. GM_addStyle    **注入CSS**

7. Promise

8. 原型，原型链

## 3. 一些网站

1. [油猴中文网](https://bbs.tampermonkey.net.cn/)
2. [油猴教程](https://learn.scriptcat.org/%E6%B2%B9%E7%8C%B4%E6%95%99%E7%A8%8B/)
3. 黑夜模式插件 darkmode
4. [css选择器](https://developer.mozilla.org/zh-CN/docs/Web/CSS/CSS_selectors)