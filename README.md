# study_docs

### 当前内容

1. [markdown](https://gitee.com/myclms/study/blob/master/markdown_study.md)
2. [git](https://gitee.com/myclms/study/blob/master/Git_study.md)
3. [python_crawler](https://gitee.com/myclms/study/blob/master/python_crawler.md) --- [HEU_get_lesson](https://gitee.com/myclms/study/blob/master/lesson.md)
4. [MCM/ICM(数学建模)](https://gitee.com/myclms/study/tree/master/MCMICM)
5. [ARKUI --- "天气之子"](https://gitee.com/myclms/study/blob/master/ARKUI%E5%AE%9E%E6%88%98%E4%B9%8B%E5%A4%A9%E6%B0%94%E9%A2%84%E6%8A%A5.md)
6. [AI](https://gitee.com/myclms/study/blob/master/AI%E7%A7%91%E6%99%AE.md)（[RL](https://gitee.com/myclms/study/blob/master/RL.md) , DL -- 李沐 ,FL）
7. [unity](https://gitee.com/myclms/study/blob/master/unity_RPG.md)
8. [tampermonkey](https://gitee.com/myclms/study/tree/master/tampermonkey.md)
9. [chrome_extension](https://gitee.com/myclms/chrome_extension) (比油猴脚本更强大，但是也更复杂。油猴脚本相当于扩展的内容脚本！)
10. [浏览器开发者工具](https://gitee.com/myclms/study/tree/master/f12.md)
11. [web-django](https://gitee.com/myclms/study_web), [vue](https://gitee.com/myclms/hello_vue3)

