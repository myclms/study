# 基于typora的markdown学习

# （源代码模式查看实现）

# 一级标题

## 二级标题

~~删除线~~

**加粗**

<u>下划线</u>

==高亮==

H~2~O

m^2^

:smile:

### table(ctrl+t)

|      |      |      |
| ---- | ---- | ---- |
|      |      |      |
|      |      |      |
|      |      |      |

> 学计算机学的
>
> > 是这样的

* 无序列表

1. 有序列表

### 代码块

```c
int main(){
    printf("markdown nb!");
    return 0;
}
```

### 行内代码

`java`,`C`

---

***斜体加粗***

***

[外部跳转](https://www.baidu.com)

[标题1](#一级标题)

<https://www.baidu.com>

## 图片

![image-20240107212737809](C:%5CUsers%5C27453%5CAppData%5CRoaming%5CTypora%5Ctypora-user-images%5Cimage-20240107212737809.png)

![](https://gitee.com/myclms/pictures/raw/master/20240107212833.png)



## [画图](https://mermaid.nodejs.cn/syntax/flowchart.html)

```mermaid
flowchart LR
	Start-->Stop
```